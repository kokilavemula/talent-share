var express = require('express'),
    cors = require("cors"),
    path = require('path');
    bodyParser = require('body-parser'),
    db = require('./db/index'),
    signup = require('./routes/signup'),
    project = require('./routes/projectdetails'),
    questions = require('./routes/questionandanswer'),
    mongoose = require('mongoose'),
    app = express();
app.use(bodyParser.json());
app.use(cors());
app.use('/signup',signup);
app.use('/project',project);
app.use('/questions',questions);
var port = process.env.PORT || 4000;
var server = app.listen(port, function(){
    console.log('Listening on port ' + port);
});
var swaggerDoc = require('./openapi.json');
var swaggerUi = require('swagger-ui-express');
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
