const express = require('express');
const router = express.Router();
var user = require('../resource/signupOperations');
router.post('/',user.registration);
router.post('/login',user.login);
router.get('/reg/:id',user.registrationById);
router.get('/',user.getAll);

module.exports = router;