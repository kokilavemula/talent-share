const express = require('express');
const router = express.Router();
var questionAndAnswersDetails = require('../resource/questionandanswerOperations');
router.post('/',questionAndAnswersDetails.questionAndAnswerDetails);
router.get('/project/:projectId',questionAndAnswersDetails.getAllQuestionAndAnswer);
router.get('/:id',questionAndAnswersDetails.getOneQuestionAndAnswer);
router.put('/:id',questionAndAnswersDetails.updateQuestionAndAnswer);
router.delete('/:id',questionAndAnswersDetails.deleteQuestionAndAnswer);
module.exports = router;