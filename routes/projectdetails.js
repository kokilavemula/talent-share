const express = require('express');
const router = express.Router();
var projectOverAllDetails = require('../resource/projectdetailsOperations');
router.post('/',projectOverAllDetails.createProjectDetails);
router.get('/',projectOverAllDetails.getAllProjectDetails);
router.get('/:id',projectOverAllDetails.getOneProjectDetails);
router.put('/:id',projectOverAllDetails.updateProjectDetails);
router.delete('/:id',projectOverAllDetails.deleteProjectDetails);
module.exports = router;