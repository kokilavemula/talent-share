var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var projectdetails = new Schema({
    title : {
        type : String
    },
    description: {
        type : String
    },
    requiredTools : {
        type : Array

    },
    projectOverAllCost : {
        type : String
    },
    projectFinalDelivery : {
        type : String
    },

    createdBy: String,
    status : String,
    createdOn : {
        type : Date,
        default : Date.now
      }
});
module.exports = mongoose.model('projectdetails',projectdetails);
