var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var questionandanswer = new Schema({
    actor : {
        objectType : String,
        displayName : String,
        id : String
    },
    object : {
        objectType : String
    },
    questionAndAnswers : [{
        questionNumber:String,
        answer:String
    }],
    status:String,
    projectId : String,
    AppliedOn : {
        type : Date,
        default : Date.now
    }

});
module.exports = mongoose.model('questionandanswer',questionandanswer);
