var frisby = require('frisby');
var Joi = frisby.Joi;
var interpolate = require('interpolate');
var config = require('../../config/development');
var projectUrl = config.url.project;
// console.log('project url is',projectUrl);
var idForOperation;
const req = {
    headers:{
        "Accept":"application/Json"
    },
    body:{
        project:{
            "title" :"Ticket Management", 
            "description":"handling tickets and give solution for that tickets",   
            "requiredTools" :["angular,node,mongodb"] ,
            "projectOverAllCost" : "30,000",
            "projectFinalDelivery" : "3 months from starting of project",
            "status" : "Applied"
        },
        updateProjectDetails: {
            "title" :"Ticket Management", 
            "description":"handling tickets and give solution for that tickets",   
            "requiredTools" :["angular,node,mongodb"] ,
            "projectOverAllCost" : "30,000",
            "projectFinalDelivery" : "3 months from starting of project",
            "status" : "Applied"
             
        }
    }
        
    
}
frisby.globalSetup({
    request:{
        headers:req.headers
    }
})
var result = interpolate('{url}',{url:projectUrl},{ delimiter: '{}'});
// console.log('project url result',result);
describe('API testing for project using frisby', function () {
    it('create new project', function (done) {
      return frisby.post(`${result}`,req.body.project,{json : true})
        .expect('status', 200)
        .expect('header','content-type','application/json; charset=utf-8')
        .expect('jsonTypes','message',{
            title: Joi.string().required(),
            requiredTools: Joi.array().required(),
            projectOverAllCost: Joi.string().required(),
            projectFinalDelivery: Joi.string().required(),
            status: Joi.string().required(),
            description: Joi.string().required()
        })
        .then((createProject)=> {
            idForOperation = createProject._json.message._id;
            expect(createProject).not.toBeNull();
            expect(createProject._json.message.title).toBe(req.body.project.title);
            expect(createProject._json.message.requiredTools).toEqual(req.body.project.requiredTools);
            expect(createProject._json.message.projectOverAllCost).toBe(req.body.project.projectOverAllCost);
            expect(createProject._json.message.projectFinalDelivery).toBe(req.body.project.projectFinalDelivery);
            expect(createProject._json.message.status).toBe(req.body.project.status);
            expect(createProject._json.message.createdBy).toBe(req.body.project.createdBy);
            expect(createProject._json.message.description).toBe(req.body.project.description);
        })
        .done(done); 
    })
    it('get All project ', function (done) {
        return frisby.get(`${result}`)
        .expect('status',200)
        .expect('header','content-type','application/json; charset=utf-8')
        .then(getAllProjectDetails=>{
            var data = getAllProjectDetails._json.message;
            expect(getAllProjectDetails).not.toBeNull();
            expect(data.length).toBeGreaterThan(0);
        })
        .done(done);     
    })
    it('should get one project details depends on id:', function (done) {
        return frisby.get(`${result}/${idForOperation}`)
        .expect('status',200)
        .expect('header','content-type','application/json; charset=utf-8')
        .then(getOneprojectDetails=>{
            expect(getOneprojectDetails).not.toBeNull();
            expect(getOneprojectDetails._json.message._id).toBe(idForOperation);
        })
        .done(done);     
    })
    it('update project detaols by id:', function (done) {
        return frisby.put(`${result}/${idForOperation}`,req.body.updateprojectDetails)
        .expect('status',200)
        .expect('header','content-type','application/json; charset=utf-8')
        .then(updateProjectDetails=>{
            expect(updateProjectDetails).not.toBeNull();
            expect(updateProjectDetails._json.message._id).toBe(idForOperation);
        })
        .done(done);     
    })
    it('delete project details by id:', function (done) {
        return frisby.del(`${result}/${idForOperation}`)
        .expect('status',200)
        .expect('header','content-type','application/json; charset=utf-8')
        .then(deleteProjectDetails=>{
            expect(deleteProjectDetails).not.toBeNull();
        })
        .done(done);     
    })
});