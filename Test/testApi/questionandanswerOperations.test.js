var frisby = require('frisby');
var Joi = frisby.Joi;
var interpolate = require('interpolate');
var config = require('../../config/development');
var questionandanswerUrl = config.url.questionandanswer;
// console.log('project url is',questionandanswerUrl);
var idForOperation;
const req = {
    headers:{
        "Accept":"application/Json"
    },
    body:{
        questionandanswers:{
            "actor":{
                "objectType":"person",
                 "displayName":"kokila",
                  "id":"2"
               },
                "object" : {
                 "objectType":"how much time you will spend for this project"
             },
             "questionAndAnswers": [{
                 "questionNumber":"1",
                 "answer":"i work 5 hrs per day"
             }],
             "status":"applied",
             "projectId":"5cae2ff3bf0e83195415a60f"
         },
         
        updatequestionandanswersDetails: {
            "actor":{
                "objectType":"person",
                 "displayName":"kokila",
                  "id":"2"
               },
                "object" : {
                 "objectType":"do you have any experience in this mention tools"
             },
             "questionAndAnswers": [{
                 "questionNumber":"2",
                 "answer":"yes,i have 2 years experience"
             }],
             "status":"applied",
             "projectId":"5cae2ff3bf0e83195415a60f"
        }            
    }       
}
frisby.globalSetup({
    request:{
        headers:req.headers
    }
})
var result = interpolate('{url}',{url:questionandanswerUrl},{ delimiter: '{}'});
// console.log('project url result',result);
describe('API testing for questionandanswer using frisby', function () {
    it('create new project', function (done) {
      return frisby.post(`${result}`,req.body.questionandanswers,{json : true})
        .expect('status', 200)
        .expect('header','content-type','application/json; charset=utf-8')
        .expect('jsonTypes','message',{
            // status: Joi.string().required(),

        })
        .then((createQuestionAndAnswer)=> {
            idForOperation = createQuestionAndAnswer._json.message._id;
            expect(createQuestionAndAnswer).not.toBeNull();
            // expect(createQuestionAndAnswer._json.message.status).toBe(req.body.questionandanswers.status);
        })
        .done(done); 
    })
    it('get All project ', function (done) {
        return frisby.get(`${result}/project/projectId`)
        .expect('status',200)
        .expect('header','content-type','application/json; charset=utf-8')
        .then(getAllProjectDetails=>{
            var data = getAllProjectDetails._json.message;
            expect(getAllProjectDetails).not.toBeNull();
            // expect(data.length).toBeGreaterThan(0);
        })
        .done(done);     
    })
    it('should get one project details depends on id:', function (done) {
        return frisby.get(`${result}/${idForOperation}`)
        .expect('status',200)
        .expect('header','content-type','application/json; charset=utf-8')
        .then(getOneProjectDetails=>{
            expect(getOneProjectDetails).not.toBeNull();
            expect(getOneProjectDetails._json.message._id).toBe(idForOperation);
        })
        .done(done);     
    })
    it('update question and answers by id:', function (done) {
        return frisby.put(`${result}/${idForOperation}`,req.body.updatequestionandanswersDetails)
        .expect('status',200)
        .expect('header','content-type','application/json; charset=utf-8')
        .then(updateQuestionAndAnswerDetails=>{
            expect(updateQuestionAndAnswerDetails).not.toBeNull();
            expect(updateQuestionAndAnswerDetails._json.message._id).toBe(idForOperation);
        })
        .done(done);     
    })
    it('delete question and answer by id:', function (done) {
        return frisby.del(`${result}/${idForOperation}`)
        .expect('status',200)
        .expect('header','content-type','application/json; charset=utf-8')
        .then(deletequestionandanswerDetails=>{
            expect(deletequestionandanswerDetails).not.toBeNull();
        })
        .done(done);     
    })
})