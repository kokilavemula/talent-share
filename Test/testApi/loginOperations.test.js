var frisby = require('frisby');
var Joi = frisby.Joi;
var interpolate = require('interpolate');
var config = require('../../config/development')
var loginUrl = config.url.signup;
var idForOperation; 
const req = {
    headers:{
        "Accept":"application/Json"
    },
    body:{
        login:{
            "email":"kokila1@gmail.com",
            "password":"kokila1"
        },
    }
}
frisby.globalSetup({
    request:{
        headers:req.headers
    }
})
var result = interpolate('{url}',{url:loginUrl},{ delimiter: '{}'});
console.log('login url result',result);
describe('API testing for login using frisby', function () {
    it('login', function (done) {
      return frisby.post(`${result}/login`,req.body.login,{json : true})
        .expect('status', 200)
        .expect('header','content-type','application/json; charset=utf-8')
        .expect('jsonTypes',{
            email:Joi.string(),
            password:Joi.string(),
        })
        .then((loginUser)=> {
            expect(loginUser).not.toBeNull()
        })
        .done(done); 
    })
})    