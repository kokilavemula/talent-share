var questionAndAnswer = require('../model/questionandanswer');
const questionAndAnswerDetails = (req, res)=>{
  console.log('req data',req.body);
    return questionAndAnswer.create(req.body,function(error,data){
     
        if(error){
          return res.send({
            status : 400,
            message : 'bad request'
          });
        }
        else{
            console.log('created data',data);
            console.log('req data',req.body);
          return res.send({
            status : 200,
            message : data
          })
        }
    })
}
const getAllQuestionAndAnswer = (req, res)=>{
  return questionAndAnswer.find({projectId:req.params.projectId},function(error,data){
      if(error){
        return res.send({
          status : 400,
          message : 'bad request'
        });
      }
      else{
          console.log('getdata for question is',data);
        return res.send({
          status : 200,
          message : data
        })
      }
  })
}
var getOneQuestionAndAnswer = function(req,res){
  return questionAndAnswer.findById(req.params.id,function(error,data){
      if(error){
          return res.send({
              statusCode : 400,
              message : error
          });
      }
      else{
         console.log('getOneQuestionAndAnswer details',data);
          res.send({
              statusCode : 200,
              message : data
          })
      }
  })
}
var updateQuestionAndAnswer = function(req,res){
  console.log('req data',req.body);
  console.log('req.params.id',req.params.id);
    return questionAndAnswer.findByIdAndUpdate(req.params.id,req.body,function(error,data){
      // console.log('req data',req.body);
        if(!error){
            console.log('data from put method : ',data);
            res.send({
                statusCode : 200,
                message : data
            });
        }
        else{
          //  console.log('error : ',error);
            res.send({
                statusCode : 500,
                error : error
            })
        }
    })
}
var deleteQuestionAndAnswer = function(req,res){
  // console.log('delete method');
  return questionAndAnswer.findByIdAndRemove(req.params.id, function (error, data) {
      if (error) return res.send({statusCode : 400});
      res.send({
          statusCode : 200,
          message : "deleted"
      });
      console.log('deleted');
  });
}
exports.questionAndAnswerDetails = questionAndAnswerDetails;
exports.getAllQuestionAndAnswer = getAllQuestionAndAnswer;
exports.getOneQuestionAndAnswer = getOneQuestionAndAnswer;
exports.updateQuestionAndAnswer = updateQuestionAndAnswer;
exports.deleteQuestionAndAnswer = deleteQuestionAndAnswer;