var express = require('express');
// var app = express();
var jwt = require('jsonwebtoken');
var config = require('config');
var userRoutes = express.Router()
var bcrypt = require('bcryptjs');
var saltRounds = 10;
// Require Item model in our routes module
var User = require('../model/employeeSignUp');
var registration = (req, res)=>{
let user = req.body;
    let password = user.password;

    return bcrypt.hash(password,saltRounds,function(err,hash){
      let newPassword = hash;
      user.password = newPassword;
      console.log(user.password);
      console.log('register details',user);
      return User.create(user,function(error,data){
        if(error){
          return res.send({
            status : 400,
            message : error
          });
        }
        else{
          return res.send({
            status : 200,
            message : data
          })
        }
    })
  
     })

};
const login = (req, res)=>{
  var password = req.body.password;
  var email = req.body.email;
  console.log('email is:',email);
  return User.findOne({email: req.body.email}, function (err, users){
  
    if(err){
        // console.log('err in if con:',err);
        // return res.send(err);   
        return res.send({
          status: 400,
          message: err
        });   
    }
    else{
      if(req.body.email !==users.email){
        console.log('err in else con is invalid email id');
        return res.send({
          status: 401,
          message: 'invalid email'
        });
      } else {
  
        let hash = users.password;
                bcrypt.compare(password,hash,function(err,resData){
                  // console.log("res : ",resData);
                    if(resData == false)
                    {
                        return res.send({
                            status : 401,
                            message :'wrong password, please try again'
                        });
                    }
                    else{
                        // console.log('before payload:',users._id); 
                        let payload = { email : users.email, id : users._id };
                        // console.log('payload id:',payload)
                        let token = jwt.sign(payload, 'keyfortoken');
                        return res.send({ 
                            status:200,
                            token : token
                        });
                        
                    }
                  })
              }
          }
      })
  };
    const registrationById = (req, res)=>{
    return User.findById(req.params.id, function (err, users){
      // console.log(users._id);
      if(err){
        console.log(err);
      }
      else {
      console.log('users data:',users);
     return res.json(users);
      
      }
    }); 
 };
 const getAll = (req, res)=>{
    return User.find(function(error,data){
      if(error){
        return res.send({
          status : 400,
          message : 'bad request'
        });
      }
      else{
        return res.send({
          status : 200,
          message : data
        })
      }
  })
};

exports.registration = registration;
exports.login = login;
exports.registrationById = registrationById;
exports.getAll = getAll;