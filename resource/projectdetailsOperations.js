var projectDetails = require('../model/projectdetails');
var config = require('config');
const createProjectDetails = (req, res)=>{
    // console.log('id is',config.tenantId);
    let userData = req.body;
    // userData.tenantId = config.tenantId;
    //  console.log(userData);
    //  console.log('req from mocha testing',req);
    return projectDetails.create(userData,function(error,data){
        if(error){
          return res.send({
            status : 400,
            message : 'bad request'
          });
        }
        else{
            console.log('created data',data)
          return res.send({
            status : 200,
            message : data
          })
        }
    })
}
const getAllProjectDetails = (req, res)=>{
    return projectDetails.find(function(error,data){
        if(error){
          return res.send({
            status : 400,
            message : 'bad request'
          });
        }
        else{
            console.log('getdata is',data);
          return res.send({
            status : 200,
            message : data
          })
        }
    })
}
const getOneProjectDetails = function(req,res){
  return projectDetails.findById(req.params.id,function(error,data){
      if(error){
          return res.send({
              statusCode : 400,
              message : error
          });
      }
      else{
         console.log('getOneproject details',data);
          res.send({
              statusCode : 200,
              message : data
          })
      }
  })
}
var updateProjectDetails = function(req,res){
  return projectDetails.findByIdAndUpdate(req.params.id,req.body,function(error,data){
      if(!error){
          // console.log('data from put method : ',data);
          res.send({
              statusCode : 200,
              message : data
          });
      }
      else{
          // console.log('error : ',error);
          res.send({
              statusCode : 500,
              error : error
          })
      }
  })
}
var deleteProjectDetails = function(req,res){
// console.log('delete method');
return projectDetails.findByIdAndRemove(req.params.id, function (error, data) {
    if (error) return res.send({statusCode : 400});
    res.send({
        statusCode : 200,
        message : "deleted"
    });
    console.log('deleted');
});
}
exports.createProjectDetails = createProjectDetails;
exports.getAllProjectDetails = getAllProjectDetails;
exports.getOneProjectDetails = getOneProjectDetails;
exports.updateProjectDetails = updateProjectDetails;
exports.deleteProjectDetails = deleteProjectDetails;
